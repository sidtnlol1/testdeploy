#!/bin/bash

ADMIN_NAME="admin"
ADMIN_EMAIL="admin@myproject.com"
PASSWORD="password"


python3 manage.py makemigrations
python3 manage.py migrate
python3 manage.py loaddata professions.json
echo "from django.contrib.auth import get_user_model; User = get_user_model(); User.objects.create_superuser('$ADMIN_NAME', '$ADMIN_EMAIL', '$PASSWORD')" | python3 manage.py shell
python3 manage.py collectstatic
gunicorn  employee_base.wsgi:application --bind 0.0.0.0:8000

